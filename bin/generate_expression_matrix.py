import logging
import sys
from collections import Counter
from pathlib import Path
import os
import pandas as pd
import argparse

logger = logging.getLogger()

def get_path(path, file):
    res_path = os.path.join(path, file)
    if not os.path.isfile(res_path):
        raise ValueError(f"file {res_path} does not exist")
    return res_path
 
def generate_matrix(samples, count_file, file_out):
    sample_list = samples.strip().split(",")
    sample_files = [get_path(path, count_file) for path in sample_list]
    sample_dfs = [pd.read_csv(path, sep="\t", index_col=[0], usecols=[0,2]) for path in sample_files]
    merged_df = pd.concat(sample_dfs, axis = 1)
    merged_df.to_csv(file_out, sep="\t")

def parse_args(argv=None):
    """Define and immediately parse command line arguments."""
    parser = argparse.ArgumentParser(
        description="Generate expression matrix from individual samples.",
        epilog="Example: python generate_expression_matrix.py --samples sample1/,sample2/,sample3/ --count_file star_salmon/salmon.merged.gene_counts.tsv --file_out abundances.csv",
    )
    parser.add_argument(
        "--samples",
        help="Comma-separated list of sample pathes",
    )   
    parser.add_argument(
        "--count_file",
        help="Path to the count file inside of the samples",
    )
    parser.add_argument(
        "--file_out",
        type=Path,
        help="Abundance matrix in CSV format.",
    )
    parser.add_argument(
        "-l",
        "--log-level",
        help="The desired log level (default WARNING).",
        choices=("CRITICAL", "ERROR", "WARNING", "INFO", "DEBUG"),
        default="WARNING",
    )
    return parser.parse_args(argv)

def main(argv=None):
    """Coordinate argument parsing and program execution."""
    args = parse_args(argv)
    logging.basicConfig(level=args.log_level, format="[%(levelname)s] %(message)s")
    args.file_out.parent.mkdir(parents=True, exist_ok=True)
    generate_matrix(args.samples, args.count_file, args.file_out)

if __name__ == "__main__":
    sys.exit(main())
