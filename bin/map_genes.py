#!/usr/bin/env python

from pyensembl import EnsemblRelease
import pandas as pd
import argparse

parser = argparse.ArgumentParser(description='config generator')


parser.add_argument('--input', help='input file path')
parser.add_argument('--output', help='output file path')
parser.add_argument('--release', help='ensembl release')
parser.add_argument('--sep', help='separator')
parser.add_argument('--map_from', help='map fron field')
parser.add_argument('--map_to', help='map to field')

args = parser.parse_args()
print(args)
data = EnsemblRelease(args.release)

genes = data.genes()
gene_map = {}
for gene in genes:
   gene_map[getattr(gene,args.map_from)] = getattr(gene,args.map_to)

input_df = pd.read_csv(args.input, sep = "\t")
input_df.iloc[:,0] = input_df.iloc[:,0].map(gene_map)
input_df.dropna(subset = input_df.columns[0], inplace = True)

input_df.to_csv(args.output, sep = "\t", index = False)
