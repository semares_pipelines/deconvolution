#!/bin/bash

while getopts ":-:" o; do
    case "${o}" in
      -)
          case "${OPTARG}" in
                samples)
                    samples="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
                    ;;
                count_file)
                    count_file="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
                    ;;
                training_files)
                    training_files="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
                    ;;
                training_names)
                    training_names="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
                    ;;
                ensembl_release)
                    ensembl_release="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
                    ;;
                matrix_sep)
                    matrix_sep="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
                    ;;
                matrix_map_gene_id_from)
                    matrix_map_gene_id_from="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
                    ;;
                matrix_map_gene_id_to)
                    matrix_map_gene_id_to="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
                    ;;
                map_gene_ids)
                    map_gene_ids="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
                    ;;
                outdir)
                    outdir="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
                    ;;
                *)
                    OPTIND=$(( $OPTIND + 1 )) 
          esac;;
    esac
done
shift $((OPTIND-1))

SCRIPT=$(realpath "$0")
SCRIPTPATH=$(dirname "$SCRIPT")

DATAPATH="/projectbig/semares/semares-test2.int.ims.bio/persistence/data/"

docker run -v $PWD:$PWD -v /var/run/docker.sock:/var/run/docker.sock -v $SCRIPTPATH:$SCRIPTPATH -v $DATAPATH:$DATAPATH amancevice/pandas bash -c "python3 $SCRIPTPATH/bin/generate_expression_matrix.py \\
           --samples $samples \\
           --count_file $count_file \\
           --file_out $PWD/abundances.txt
"
      
nextflow run $SCRIPTPATH/main.nf \
         --matrix "$PWD/abundances.txt" \
         --training_files "$training_files" \
         --training_names "$training_names" \
         --outdir $outdir \
         --ensembl_release $ensembl_release \
         --matrix_sep $matrix_sep \
         --matrix_map_gene_id_from $matrix_map_gene_id_from \
         --matrix_map_gene_id_to $matrix_map_gene_id_to \
         --map_gene_ids $map_gene_ids \
"      
