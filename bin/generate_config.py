#!/usr/bin/env python

from jinja2 import Environment, FileSystemLoader
import os
import argparse

parser = argparse.ArgumentParser(description='config generator')

parser.add_argument('--new_config', help='new config path')
parser.add_argument('--test_dataset', help='test dataset path')
parser.add_argument('--reference_data', help='reference data path')
parser.add_argument('--experiment_name', help='name of the experiment')

args = parser.parse_args()

environment = Environment(loader=FileSystemLoader(os.path.dirname(os.path.realpath(__file__))))
conf_template = environment.get_template("main_config.jinja")

conf = conf_template.render(
          test_dataset = args.test_dataset,
          reference_data = args.reference_data,
          experiment_name = args.experiment_name)

with open(args.new_config, mode="w", encoding="utf-8") as new_conf_f:
    new_conf_f.write(conf)


#with open(args.default_config, "r") as stream:
#    try:
#        conf = yaml.safe_load(stream)
#        print(conf)
#    except yaml.YAMLError as exc:
#        print(exc)

#print(conf)

#with open(args.new_config, 'w') as file:
#    documents = yaml.dump(config, file)

