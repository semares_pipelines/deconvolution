nextflow.enable.dsl = 2
params.map_gene_ids = ""

process GENERATE_MATRIX(){
    input:
       file(matrix)

    output:
       path("${matrix}_mapped.csv"), emit: matrix

    """
       cp $matrix "${matrix}_mapped.csv"
    """
}

process MAP_GENE_IDS{
    container "semares:dissect" // use docker conatainer
    containerOptions '--volume /root:/root'

    input:
       file(matrix)
       val ensembl_release
       val matrix_sep 
       val matrix_map_gene_id_from 
       val matrix_map_gene_id_to
    output:
       path("${matrix}_mapped.csv"), emit: matrix

    """
      pyensembl install --release $ensembl_release --species homo_sapiens
      map_genes.py \\
           --input "$matrix" \\
           --release $ensembl_release \\
           --output "${matrix}_mapped.csv" \\
           --sep '\\t' \\
           --map_from $matrix_map_gene_id_from \\
           --map_to $matrix_map_gene_id_to
    """
}

process DISSECT_PREPARE_DATA{
    container "semares:dissect" // use docker conatainer

    input:
       file(matrix)
       tuple val(training_ind), file(training_f), val(training_name)
    output:
       path("configs"), emit: config
       path(training_name), emit: experiment
       path(training_f), emit: training
    """
       mkdir configs
       generate_config.py \\
          --new_config ./configs/main_config.py \\
          --test_dataset $matrix \\
          --reference_data $training_f \\
          --experiment_name $training_name
       cp /dissect/DISSECT/prepare_data.py ./
       python3 prepare_data.py
    """
}

process DISSECT_DISSECT{
    container "semares:dissect" // use docker conatainer

    input:
       file(matrix)
       file(training_f)
       path(config)
       path(training_name)
    output:
       path(training_name), emit: experiment

    """
       cp /dissect/DISSECT/dissect.py ./
       cp -R /dissect/DISSECT/utils ./
       python3 dissect.py
    """
}

process DISSECT_EXPLAIN{
    container "semares:dissect" // use docker conatainer

    input:
       file(matrix)
       file(training_f)
       path(config)
       path(experiment)
    output:
       path(experiment), emit: experiment

    """
       cp /dissect/DISSECT/explain.py ./
       cp -R /dissect/DISSECT/utils ./
       python3 explain.py
    """
}

workflow DISSECT {
     input_matrix_ch = Channel.fromPath(params.matrix).first()
     GENERATE_MATRIX (input_matrix_ch)
     if (params.map_gene_ids){
        MAP_GENE_IDS(
            GENERATE_MATRIX.out.matrix,
            params.ensembl_release,
            params.matrix_sep,
            params.matrix_map_gene_id_from,
            params.matrix_map_gene_id_to
        )
        matrix_new_file = MAP_GENE_IDS.out.matrix 
    }else{
        matrix_new_file = GENERATE_MATRIX.out.matrix  
    }
   print(params.training_files)
   file_list = params.training_files.tokenize(",").withIndex().collect { item, index -> ["index": index, "file": item]}
   name_list = params.training_names.tokenize(",").withIndex().collect { item, index -> ["index": index, "name": item]}
   println(file_list)
   println(name_list)
   exp_names = Channel
        .fromList(name_list)
        .map{it->[it.index, it.name]}
   training_ch = Channel
        .fromList(file_list)
        .map{it->[it.index, file(it.file)]}
        .combine(exp_names, by: 0)
        .view()
  
   DISSECT_PREPARE_DATA (
          matrix_new_file,
          training_ch         
          )
   DISSECT_DISSECT (
          matrix_new_file,
          DISSECT_PREPARE_DATA.out.training,
          DISSECT_PREPARE_DATA.out.config,
          DISSECT_PREPARE_DATA.out.experiment
          )
   DISSECT_EXPLAIN (
          matrix_new_file,
          DISSECT_PREPARE_DATA.out.training,
          DISSECT_PREPARE_DATA.out.config,
          DISSECT_DISSECT.out.experiment
          )
}

/*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    RUN ALL WORKFLOWS
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*/

//
// WORKFLOW: Execute a single named workflow for the pipeline
// See: https://github.com/nf-core/rnaseq/issues/619
//



workflow {
    DISSECT ()
}

/*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    THE END
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*/
